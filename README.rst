cslbot-tjhsst
=============

tjhsst-specific commands for https://pypi.python.org/pypi/CslBot


To install:

    pip install git+git://gitlab.com/tjcsl/cslbot-tjhsst

    add "extramodules: cslbot-tjhsst" to the [core] section of config.cfg
